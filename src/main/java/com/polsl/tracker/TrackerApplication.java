package com.polsl.tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.polsl.controllers")
@SpringBootApplication
public class TrackerApplication {
	public static void main(String[] args) {
//		CudaEnvironment.getInstance().getConfiguration()
//				.allowMultiGPU(true)
//				.setMaximumDeviceCache(2L * 1024L * 1024L * 1024L)
//				.allowCrossDeviceAccess(true);
		SpringApplication.run(TrackerApplication.class, args);
	}
}
