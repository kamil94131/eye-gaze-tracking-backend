package com.polsl.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.polsl.configuration.ApplicationConfiguration;
import com.polsl.configuration.NeuralNetworkConfiguration;
import com.polsl.models.*;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@RestController
public class EyeImageController {
    private static final String DESKTOP_NAME = "Desktop";
    private static final String USER_DESKTOP_PATH = String.format("%s%s%s", System.getProperty("user.home"), File.separator, DESKTOP_NAME);
    private static final String MERGED_EYES_FOLDER_NAME = "mergedEyes";
    private static final String NEURAL_NETWORK_MODEL_FOLDER_NAME = "neuralNetworkModel";
    private static final String ZIP_EXTENSION = ".zip";
    private static final Logger logger = LoggerFactory.getLogger(EyeImageController.class);
    private NeuralNetworkConfiguration neuralNetworkConfiguration = new NeuralNetworkConfiguration();
    private ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration();
    private MultiLayerNetwork model;
    private INDArray labels;

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path="/saveGreyScaleEyes", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> saveGreyScaleEyes(@RequestBody String input) throws IOException {
        Type type = new TypeToken<List<GreyScaleEyes>>(){}.getType();
        List<GreyScaleEyes> greyScaleEyes = new Gson().fromJson(input, type);

        final List<EyeImage> eyeImages = EyeImage.convertToEyeImage(greyScaleEyes);
        final File leftEyeFolder = EyeImageFileUtils.generateFolders("leftEyeFolder");
        final File rightEyeFolder = EyeImageFileUtils.generateFolders("rightEyeFolder");
        Integer leftEyeIteration = 0;
        Integer rightEyeIteration = 0;

        for(EyeImage eyeImage : eyeImages){
            final BufferedImage greyScaleLeftEyeImage = EyeImage.flipHorizontaly(EyeImage.equalizeHistogram(EyeImage.toImage(eyeImage.getLeftEyeGreyScalePixels(), ApplicationConfiguration.WIDTH, ApplicationConfiguration.HEIGHT)));
            final BufferedImage greyScaleRightEyeImage = EyeImage.flipHorizontaly(EyeImage.equalizeHistogram(EyeImage.toImage(eyeImage.getRightEyeGreyScalePixels(), ApplicationConfiguration.WIDTH, ApplicationConfiguration.HEIGHT)));
            File leftEyeImage = EyeImageFileUtils.generateImage(leftEyeIteration, leftEyeFolder.getAbsolutePath(), eyeImage.getId());
            File rightEyeImage = EyeImageFileUtils.generateImage(rightEyeIteration, rightEyeFolder.getAbsolutePath(), eyeImage.getId());
            ImageIO.write(greyScaleLeftEyeImage, EyeImage.IMAGE_EXTENSION, leftEyeImage);
            ImageIO.write(greyScaleRightEyeImage, EyeImage.IMAGE_EXTENSION, rightEyeImage);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path="/getMergedEyesGeneratedFolders", produces = "application/json")
    public List<Integer> getMergedEyesGeneratedFolders(){
        return EyeImageFileUtils.getMergedEyesGeneratedFolders(MERGED_EYES_FOLDER_NAME, neuralNetworkConfiguration.getOutputNumber());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path="/getCrossValidationResult", produces = "application/json")
    public List<CrossValidationResult> getCrossValidationResult() throws IOException {
        final List<Integer> mergedEyesGeneratedFolders = getMergedEyesGeneratedFolders();
        if(mergedEyesGeneratedFolders.isEmpty()){
            return Collections.emptyList();
        }

        final List<CrossValidationResult> crossValidationResults = new ArrayList<>();
        for(Integer folderId : mergedEyesGeneratedFolders){
            final List<BufferedImage> mergedEyesImages = EyeImageFileUtils.getAllImagesInsideFolder(EyeImageFileUtils.getMergedEyesFolder(MERGED_EYES_FOLDER_NAME, folderId));
            final CrossValidationResult crossValidationResult = new CrossValidationResult();
            for(BufferedImage mergedEyesImage : mergedEyesImages){
                NativeImageLoader loader = new NativeImageLoader(ApplicationConfiguration.MERGED_HEIGHT, ApplicationConfiguration.MERGED_WIDTH, neuralNetworkConfiguration.getChannel());
                INDArray mergedEyesEvaluation = loader.asMatrix(mergedEyesImage);
                DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
                scaler.transform(mergedEyesEvaluation);

                INDArray output = model.output(mergedEyesEvaluation);
                double maxValue = -1.0;
                int maxIndex = 0;
                for(int i = 0; i < output.toDoubleVector().length; i++){
                    if(output.toDoubleVector()[i] > maxValue){
                        maxValue = output.getColumn(i).toDoubleVector()[0];
                        maxIndex = i;
                    }
                }
                if(maxIndex == folderId){
                    crossValidationResult.incrementPositiveValuation();
                }else{
                    crossValidationResult.incrementNegativeValuation();
                }
            }
            crossValidationResult.setFolderId(folderId.intValue());
            crossValidationResult.setFolderQuantity(mergedEyesImages.size());
            crossValidationResults.add(crossValidationResult);
        }
        return crossValidationResults;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path="/saveMergedGreyScaleEyes", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> saveMergedGreyScaleEyes(@RequestBody String input) throws IOException {
        Type type = new TypeToken<List<GreyScaleEyes>>(){}.getType();
        List<GreyScaleEyes> greyScaleEyes = new Gson().fromJson(input, type);

        final List<EyeImage> eyeImages = EyeImage.convertToEyeImage(greyScaleEyes);
        final File mergedEyeFolder = EyeImageFileUtils.generateFolders("mergedEyes");
        Integer mergedEyeIteration = 0;

        for(EyeImage eyeImage : eyeImages){
            final BufferedImage greyScaleLeftEyeImage = EyeImage.equalizeHistogram(EyeImage.toImage(eyeImage.getLeftEyeGreyScalePixels(), ApplicationConfiguration.WIDTH, ApplicationConfiguration.HEIGHT));
            final BufferedImage greyScaleRightEyeImage = EyeImage.equalizeHistogram(EyeImage.toImage(eyeImage.getRightEyeGreyScalePixels(), ApplicationConfiguration.WIDTH, ApplicationConfiguration.HEIGHT));
            final BufferedImage mergedEyes = EyeImage.flipHorizontaly(EyeImage.mergeImages(applicationConfiguration.getMergeDirection(), greyScaleLeftEyeImage, greyScaleRightEyeImage));

            File mergedEyeImage = EyeImageFileUtils.generateImage(mergedEyeIteration, mergedEyeFolder.getAbsolutePath(), eyeImage.getId());
            ImageIO.write(mergedEyes, EyeImage.IMAGE_EXTENSION, mergedEyeImage);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path="/predictGazePosition", consumes = "application/json", produces = "application/json")
    public Integer predictGazePosition(@RequestBody String input) throws IOException {
        synchronized(this){
            Type type = new TypeToken<GreyScaleEyes>(){}.getType();
            GreyScaleEyes greyScaleEye = new Gson().fromJson(input, type);
            EyeImage eyeImage = EyeImage.convertToEyeImage(greyScaleEye);

            final BufferedImage greyScaleLeftEyeImage = EyeImage.equalizeHistogram(EyeImage.toImage(eyeImage.getLeftEyeGreyScalePixels(), ApplicationConfiguration.WIDTH, ApplicationConfiguration.HEIGHT));
            final BufferedImage greyScaleRightEyeImage = EyeImage.equalizeHistogram(EyeImage.toImage(eyeImage.getRightEyeGreyScalePixels(), ApplicationConfiguration.WIDTH, ApplicationConfiguration.HEIGHT));
            final BufferedImage mergedEyes = EyeImage.flipHorizontaly(EyeImage.mergeImages(applicationConfiguration.getMergeDirection(), greyScaleLeftEyeImage, greyScaleRightEyeImage));
            NativeImageLoader loader = new NativeImageLoader(ApplicationConfiguration.MERGED_HEIGHT, ApplicationConfiguration.MERGED_WIDTH, neuralNetworkConfiguration.getChannel());
            INDArray mergedEyesEvaluation = loader.asMatrix(mergedEyes);
            DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
            scaler.transform(mergedEyesEvaluation);

            INDArray output = model.output(mergedEyesEvaluation);
            double maxValue = -1.0;
            int maxIndex = 0;
            for(int i = 0; i < output.toDoubleVector().length; i++){
                if(output.toDoubleVector()[i] > maxValue){
                    maxValue = output.getColumn(i).toDoubleVector()[0];
                    maxIndex = i;
                }
            }

            return this.applicationConfiguration.isUseMedianFilter() ? MedianFilter.filter(maxIndex) : maxIndex;
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path="/isNeuralNetworkModelTrained")
    public Boolean isNeuralNetworkModelTrained() {
        return this.model != null;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path="/startLearningProcess")
    public String startLearningProcess() throws IOException {
        Instant start = Instant.now();
        Random randNumGen = new Random(neuralNetworkConfiguration.getRngseed());

        File trainData = new File(String.format("%s\\%s", USER_DESKTOP_PATH, MERGED_EYES_FOLDER_NAME));
        File testData = new File(String.format("%s\\%s", USER_DESKTOP_PATH, MERGED_EYES_FOLDER_NAME));

        FileSplit train = new FileSplit(trainData, NativeImageLoader.ALLOWED_FORMATS, randNumGen);
        FileSplit test = new FileSplit(testData, NativeImageLoader.ALLOWED_FORMATS, randNumGen);

        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
        ImageRecordReader recordReader = new ImageRecordReader(ApplicationConfiguration.MERGED_HEIGHT, ApplicationConfiguration.MERGED_WIDTH, neuralNetworkConfiguration.getChannel(), labelMaker);

        try {
            recordReader.initialize(train);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DataSetIterator dataIter = new RecordReaderDataSetIterator(recordReader, neuralNetworkConfiguration.getBatchSize(), 1, neuralNetworkConfiguration.getOutputNumber());
        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
        scaler.fit(dataIter);
        dataIter.setPreProcessor(scaler);

        logger.info("BUILD MODEL");
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(neuralNetworkConfiguration.getRngseed())
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(new Nesterovs(neuralNetworkConfiguration.getLearningRate(), neuralNetworkConfiguration.getMomentum()))
                .l2(1e-4)
                .list()
                .layer(0, new DenseLayer.Builder()
                        .nIn(ApplicationConfiguration.MERGED_WIDTH * ApplicationConfiguration.MERGED_HEIGHT)
                        .nOut(600)
                        .activation(Activation.TANH)
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .layer(1, new DenseLayer.Builder()
                        .nIn(600)
                        .nOut(600)
                        .activation(Activation.TANH)
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .layer(2, new DenseLayer.Builder()
                        .nIn(600)
                        .nOut(600)
                        .activation(Activation.TANH)
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .layer(3, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nIn(600)
                        .nOut(neuralNetworkConfiguration.getOutputNumber())
                        .activation(Activation.SOFTMAX)
                        .weightInit(WeightInit.RELU)
                        .build())
                .pretrain(false)
                .backprop(true)
                .setInputType(InputType.convolutional(ApplicationConfiguration.MERGED_HEIGHT, ApplicationConfiguration.MERGED_WIDTH, neuralNetworkConfiguration.getChannel()))
                .build();

        model = new MultiLayerNetwork(conf);
        model.setLabels(labels);
        model.setListeners(new ScoreIterationListener(1));

        logger.info("TRAIN MODEL");
        model.init();
        for (int i = 0; i < neuralNetworkConfiguration.getNumberEpochs(); i++) {
            model.fit(dataIter);
        }

        logger.info("EVALUATE MODEL");
        recordReader.reset();

        try {
            recordReader.initialize(test);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DataSetIterator testIter = new RecordReaderDataSetIterator(recordReader, neuralNetworkConfiguration.getBatchSize(), 1, neuralNetworkConfiguration.getOutputNumber());
        scaler.fit(testIter);
        testIter.setPreProcessor(scaler);

        logger.info(recordReader.getLabels().toString());

        Evaluation eval = new Evaluation(neuralNetworkConfiguration.getOutputNumber());

        while (testIter.hasNext()) {
            DataSet next = testIter.next();
            INDArray output = model.output(next.getFeatureMatrix());
            eval.eval(next.getLabels(), output);
        }
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        File locationToSave = new File(String.format("%s\\%s%s", USER_DESKTOP_PATH, NEURAL_NETWORK_MODEL_FOLDER_NAME, ZIP_EXTENSION));
        ModelSerializer.writeModel(model, locationToSave, true);
        logger.info(eval.stats());
        logger.info(String.valueOf(timeElapsed));
        return "scripts";
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path="/changeNeuralNetworkConfiguration", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> changeNeuralNetworkConfiguration(@RequestBody String input){
        Type type = new TypeToken<NeuralNetworkConfiguration>(){}.getType();
        final NeuralNetworkConfiguration configuration = new Gson().fromJson(input, type);
        this.neuralNetworkConfiguration.setRngseed(configuration.getRngseed());
        this.neuralNetworkConfiguration.setBatchSize(configuration.getBatchSize());
        this.neuralNetworkConfiguration.setChannels(configuration.getChannel());
        this.neuralNetworkConfiguration.setNumberEpochs(configuration.getNumberEpochs());
        this.neuralNetworkConfiguration.setOutputNumber(configuration.getOutputNumber());
        this.neuralNetworkConfiguration.setLearningRate(configuration.getLearningRate());
        this.neuralNetworkConfiguration.setMomentum(configuration.getMomentum());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path="/getNeuralNetworkConfiguration")
    public NeuralNetworkConfiguration getNeuralNetworkConfiguration(){
        return this.neuralNetworkConfiguration;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path="/loadLearnedNeuralNetwork")
    public Boolean loadLearnedNeuralNetwork() {
        try {
            this.model = ModelSerializer.restoreMultiLayerNetwork(String.format("%s\\%s%s", USER_DESKTOP_PATH, NEURAL_NETWORK_MODEL_FOLDER_NAME, ZIP_EXTENSION));
            logger.info("Successfully loaded neural network configuration!");
        } catch (IOException e) {
            logger.info("Error occurred when tried to load neural network configuration!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path="/changeApplicationConfiguration", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> changeApplicationConfiguration(@RequestBody String input){
        Type type = new TypeToken<ApplicationConfiguration>(){}.getType();
        final ApplicationConfiguration configuration = new Gson().fromJson(input, type);
        this.applicationConfiguration.setCollectingEyeDataMethod(configuration.getCollectingEyeDataMethod());
        this.applicationConfiguration.setMergeDirection(configuration.getMergeDirection());
        this.applicationConfiguration.setUseMedianFilter(configuration.isUseMedianFilter());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path="/getApplicationConfiguration")
    public ApplicationConfiguration getApplicationConfiguration(){
        logger.info(this.applicationConfiguration.toString());
        return this.applicationConfiguration;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path="/ping")
    public Boolean ping() {
        return true;
    }
}
