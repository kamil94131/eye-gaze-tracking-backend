package com.polsl.configuration;

public class ApplicationConfiguration {
    private CollectingEyeDataMethod _collecttingEyeDataMethod = CollectingEyeDataMethod.MERGED;
    private MergeDirection _mergeDirection = MergeDirection.VERTICALY;
    private Boolean _useMedianFilter = Boolean.TRUE;
    public static int WIDTH = 36;
    public static int HEIGHT = 28;
    public static int MERGED_WIDTH = 36;
    public static int MERGED_HEIGHT = HEIGHT * 2;

    public CollectingEyeDataMethod getCollectingEyeDataMethod() {
        return _collecttingEyeDataMethod;
    }

    public void setCollectingEyeDataMethod(CollectingEyeDataMethod collectingEyeDataMethod) {
        this._collecttingEyeDataMethod = collectingEyeDataMethod;
    }

    public MergeDirection getMergeDirection() {
        return _mergeDirection;
    }

    public void setMergeDirection(MergeDirection mergeDirection) {
        if(this._mergeDirection == MergeDirection.HORIZONTALY){
            MERGED_HEIGHT *= 2;
            MERGED_WIDTH /= 2;
        }else{
            MERGED_HEIGHT /= 2;
            MERGED_WIDTH *= 2;
        }
        this._mergeDirection = mergeDirection;
    }

    public Boolean isUseMedianFilter() {
        return _useMedianFilter;
    }

    public void setUseMedianFilter(Boolean _useMedianFilter) {
        this._useMedianFilter = _useMedianFilter;
    }

    @Override
    public String toString() {
        return "ApplicationConfiguration{" +
                "_collectingEyeDataMethod=" + _collecttingEyeDataMethod +
                ", _mergeDirection=" + _mergeDirection +
                ", _useMedianFilter=" + _useMedianFilter +
                '}';
    }
}
