package com.polsl.configuration;

public class NeuralNetworkConfiguration {
    private int _channel = 1;
    private int _rngseed = 256;
    private int _batchSize = 64;
    private int _outputNumber = 9;
    private int _numberEpochs = 5;
    private double _learningRate = 0.006;
    private double _momentum = 0.9;

    public int getChannel() {
        return _channel;
    }

    public void setChannels(int _channels) {
        this._channel = _channels;
    }

    public int getRngseed() {
        return _rngseed;
    }

    public void setRngseed(int _rngseed) {
        this._rngseed = _rngseed;
    }

    public int getBatchSize() {
        return _batchSize;
    }

    public void setBatchSize(int _batchSize) {
        this._batchSize = _batchSize;
    }

    public int getOutputNumber() {
        return _outputNumber;
    }

    public void setOutputNumber(int _outputNumber) {
        this._outputNumber = _outputNumber;
    }

    public int getNumberEpochs() {
        return _numberEpochs;
    }

    public void setNumberEpochs(int _numberEpochs) {
        this._numberEpochs = _numberEpochs;
    }

    public double getLearningRate() {
        return _learningRate;
    }

    public void setLearningRate(double _learningRate) {
        this._learningRate = _learningRate;
    }

    public double getMomentum() {
        return _momentum;
    }

    public void setMomentum(double _momentum) {
        this._momentum = _momentum;
    }
}
