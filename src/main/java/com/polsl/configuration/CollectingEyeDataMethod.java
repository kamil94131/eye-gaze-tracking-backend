package com.polsl.configuration;

public enum CollectingEyeDataMethod {
    SEPARATED("SEPARATED"),
    MERGED("MERGED");

    private String name;

    CollectingEyeDataMethod(final String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
