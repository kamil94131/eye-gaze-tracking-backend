package com.polsl.models;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.Collectors;

public class MedianFilter {
    private static final int QUEUE_NOT_FILLED_WITH_VALUES = -1;
    private static final int MAX_MEDIAN_VALUES = 5;
    private static final int MIDDLE_INDEX = 3;
    private static Queue<Integer> queue = new LinkedList<>();

    public static int filter(int value){
        queue.add(value);
        if(queue.size() >= MAX_MEDIAN_VALUES){
            final int median = queue.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList()).get(MIDDLE_INDEX);
            queue.remove();
            return median;
        }
        return QUEUE_NOT_FILLED_WITH_VALUES;
    }
}
