package com.polsl.models;

import com.polsl.configuration.ApplicationConfiguration;
import com.polsl.configuration.MergeDirection;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EyeImage {
    public static final String IMAGE_EXTENSION = "jpg";
    private int id;
    private byte[] leftEyeGreyScalePixels;
    private byte[] rightEyeGreyScalePixels;

    public EyeImage() {
        leftEyeGreyScalePixels = new byte[ApplicationConfiguration.MERGED_HEIGHT * ApplicationConfiguration.MERGED_WIDTH];
        rightEyeGreyScalePixels = new byte[ApplicationConfiguration.MERGED_HEIGHT * ApplicationConfiguration.MERGED_WIDTH];
    }

    public static List<EyeImage> convertToEyeImage(final List<GreyScaleEyes> greyScaleEyes){
        final List<EyeImage> eyeImages = new ArrayList<>();
        greyScaleEyes.forEach(greyScaleEye -> {
            final EyeImage eyeImage = new EyeImage();
            eyeImage.setId(greyScaleEye._eyeId);
            eyeImage.leftEyeGreyScalePixels = convertToArrayPixels(greyScaleEye._leftEye);
            eyeImage.rightEyeGreyScalePixels = convertToArrayPixels(greyScaleEye._rightEye);
            eyeImages.add(eyeImage);
        });
        return eyeImages;
    }

    public static EyeImage convertToEyeImage(final GreyScaleEyes greyScaleEye){
        final EyeImage eyeImage = new EyeImage();
        eyeImage.setId(greyScaleEye._eyeId);
        eyeImage.leftEyeGreyScalePixels = convertToArrayPixels(greyScaleEye._leftEye);
        eyeImage.rightEyeGreyScalePixels = convertToArrayPixels(greyScaleEye._rightEye);
        return eyeImage;
    }

    private static byte[] convertToArrayPixels(final Map<String, Byte> jsonEyePixels){
        byte[] byteArray = new byte[ApplicationConfiguration.MERGED_HEIGHT * ApplicationConfiguration.MERGED_WIDTH];
        int iteration = 0;
        for (Map.Entry<String, Byte> pixel : jsonEyePixels.entrySet()) {
            byteArray[iteration] = pixel.getValue();
            iteration++;
        }
        return byteArray;
    }

    public static BufferedImage mergeImages(MergeDirection direction, BufferedImage... bufferedImages){
        final BufferedImage mergedEyes;
        int iteration = 0;
        if(direction == MergeDirection.HORIZONTALY){
            mergedEyes = new BufferedImage(ApplicationConfiguration.WIDTH * 2, ApplicationConfiguration.HEIGHT, bufferedImages[0].getType());
            for(BufferedImage image : bufferedImages){
                mergedEyes.createGraphics().drawImage(image, ApplicationConfiguration.WIDTH * iteration, 0, null);
                iteration++;
            }
        }else{
            mergedEyes = new BufferedImage(ApplicationConfiguration.WIDTH, ApplicationConfiguration.HEIGHT * 2, bufferedImages[0].getType());
            for(BufferedImage image : bufferedImages){
                mergedEyes.createGraphics().drawImage(image, 0, ApplicationConfiguration.HEIGHT * iteration, null);
                iteration++;
            }
        }
        return mergedEyes;
    }

    public static BufferedImage equalizeHistogram(final BufferedImage imageToEqualize){
        BufferedImage equalizedImage = new BufferedImage(imageToEqualize.getWidth(), imageToEqualize.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster writableRaster = imageToEqualize.getRaster();
        WritableRaster equalizedImageRaster = equalizedImage.getRaster();
        int pixelsQuantity= writableRaster.getWidth() * writableRaster.getHeight();
        int[] histogram = new int[256];

        for (int x = 0; x < writableRaster.getWidth(); x++) {
            for (int y = 0; y < writableRaster.getHeight(); y++) {
                histogram[writableRaster.getSample(x, y, 0)]++;
            }
        }

        int[] chistogram = new int[256];
        chistogram[0] = histogram[0];
        for(int i = 1; i < 256; i++){
            chistogram[i] = chistogram[i-1] + histogram[i];
        }

        float[] arr = new float[256];
        for(int i=0;i<256;i++){
            arr[i] =  (float)((chistogram[i]*255.0)/(float)pixelsQuantity);
        }

        for (int x = 0; x < writableRaster.getWidth(); x++) {
            for (int y = 0; y < writableRaster.getHeight(); y++) {
                int nVal = (int) arr[writableRaster.getSample(x, y, 0)];
                equalizedImageRaster.setSample(x, y, 0, nVal);
            }
        }
        equalizedImage.setData(equalizedImageRaster);
        return equalizedImage;
    }

    public static BufferedImage flipHorizontaly(final BufferedImage bufferedImage){
        BufferedImage image = deepCopy(bufferedImage);
        AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
        tx.translate(-image.getWidth(null), 0);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        image = op.filter(bufferedImage, null);
        return image;
    }

    public static BufferedImage deepCopy(BufferedImage bufferedImage) {
        ColorModel colorModel = bufferedImage.getColorModel();
        boolean isAlphaPremultiplied = colorModel.isAlphaPremultiplied();
        WritableRaster raster = bufferedImage.copyData(null);
        return new BufferedImage(colorModel, raster, isAlphaPremultiplied, null);
    }

    public static BufferedImage toImage(byte[] data, int width, int height) {
        DataBuffer buffer = new DataBufferByte(data, data.length);
        WritableRaster raster = Raster.createInterleavedRaster(buffer, width, height, width, 1, new int[]{0}, null);
        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
        ColorModel cm = new ComponentColorModel(cs, false, true, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
        return new BufferedImage(cm, raster, false, null);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getLeftEyeGreyScalePixels() {
        return leftEyeGreyScalePixels;
    }

    public void setLeftEyeGreyScalePixels(byte[] leftEyeGreyScalePixels) {
        this.leftEyeGreyScalePixels = leftEyeGreyScalePixels;
    }

    public byte[] getRightEyeGreyScalePixels() {
        return rightEyeGreyScalePixels;
    }

    public void setRightEyeGreyScalePixels(byte[] rightEyeGreyScalePixels) {
        this.rightEyeGreyScalePixels = rightEyeGreyScalePixels;
    }
}
