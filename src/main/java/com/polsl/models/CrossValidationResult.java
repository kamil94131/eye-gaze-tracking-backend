package com.polsl.models;

public class CrossValidationResult {
    private int folderId;
    private int folderQuantity;
    private int positiveEvaluation;
    private int negativeEvaluation;

    public int getFolderId() {
        return folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public int getFolderQuantity() {
        return folderQuantity;
    }

    public void setFolderQuantity(int folderQuantity) {
        this.folderQuantity = folderQuantity;
    }

    public int getPositiveEvaluation() {
        return positiveEvaluation;
    }

    public void setPositiveEvaluation(int positiveEvaluation) {
        this.positiveEvaluation = positiveEvaluation;
    }

    public int getNegativeEvaluation() {
        return negativeEvaluation;
    }

    public void setNegativeEvaluation(int negativeEvaluation) {
        this.negativeEvaluation = negativeEvaluation;
    }

    public void incrementPositiveValuation(){
        this.positiveEvaluation++;
    }

    public void incrementNegativeValuation(){
        this.negativeEvaluation++;
    }
}
