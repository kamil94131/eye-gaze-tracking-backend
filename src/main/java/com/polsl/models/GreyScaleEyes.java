package com.polsl.models;

import java.util.Map;

public class GreyScaleEyes {
    public int _eyeId;
    public Map<String, Byte> _leftEye;
    public Map<String, Byte> _rightEye;
}
