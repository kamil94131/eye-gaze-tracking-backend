package com.polsl.models;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EyeImageFileUtils {
    public static final String DESKTOP_NAME = "Desktop";
    public static final String USER_DESKTOP_PATH = String.format("%s%s%s", System.getProperty("user.home"), File.separator, DESKTOP_NAME);

    public static File generateFolders(String folderName){
        final File file = new File(String.format("%s%s%s", USER_DESKTOP_PATH, File.separator, folderName));
        if(!file.exists()){
            file.mkdir();
        }
        return file;
    }

    public static File generateImage(Integer iteration, final String folderPath, final int id){
        generateIdFolderIfNotExist(String.valueOf(id), folderPath);
        File eyeImage = null;
        do{
            eyeImage = new File(String.format("%s%s%s%s%s%s%s",
                    folderPath,
                    File.separator,
                    String.valueOf(id),
                    File.separator,
                    String.valueOf(iteration++),
                    ".",
                    EyeImage.IMAGE_EXTENSION));
        }while(eyeImage.exists());
        return eyeImage;
    }

    public static void generateIdFolderIfNotExist(final String eyeImageId, final String folderPath){
        final File file = new File(String.format("%s%s%s", folderPath, File.separator, eyeImageId));
        if(!file.exists()){
            file.mkdir();
        }
    }

    public static boolean existsMergedEyesFolders(final String folderName){
        final Path mergedEyesFolderPath = Paths.get(String.format("%s%s%s", USER_DESKTOP_PATH, File.separator, folderName));
        return Files.exists(mergedEyesFolderPath);
    }

    public static List<Integer> getMergedEyesGeneratedFolders(final String folderName, final int outputNumber){
        if(!existsMergedEyesFolders(folderName)){
            return Collections.emptyList();
        }
        return getMergedEyesIdFolders(folderName, outputNumber);
    }

    private static List<Integer> getMergedEyesIdFolders(final String folderName, final int outputNumber){
        final List<Integer> mergedEyesIdByFolder = new ArrayList<>();
        for(int i = 0; i < outputNumber; i++){
            final Path mergedEyesFolderPath = Paths.get(String.format("%s%s%s%s%s", USER_DESKTOP_PATH, File.separator, folderName, File.separator, String.valueOf(i)));
            if(Files.exists(mergedEyesFolderPath)){
                mergedEyesIdByFolder.add(i);
            }
        }
        return mergedEyesIdByFolder;
    }

    public static File getMergedEyesFolder(final String folderName, final Integer folderId){
        final Path mergedEyesFolderPath = Paths.get(String.format("%s%s%s%s%s", USER_DESKTOP_PATH, File.separator, folderName, File.separator, String.valueOf(folderId)));
        return new File(mergedEyesFolderPath.toString());
    }

    public static List<BufferedImage> getAllImagesInsideFolder(final File file){
        File[] filesInsideFolder = file.listFiles();
        if(filesInsideFolder == null || filesInsideFolder.length == 0){
            return Collections.emptyList();
        }

        final List<BufferedImage> imagesInsideFolder = new ArrayList<>();
        for(int i = 0; i < filesInsideFolder.length; i++){
            try {
                imagesInsideFolder.add(ImageIO.read(filesInsideFolder[i]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return imagesInsideFolder;
    }
}
